import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../shared/message.model';
import { MessagesService } from '../shared/messages.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages!: Message[];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private messagesService: MessagesService) {}

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messagesService.messagesChange.subscribe((messages:Message[]) => {
      this.messages = messages.reverse();
    });
    this.messagesFetchingSubscription = this.messagesService.messagesFetching.subscribe((isFetching:boolean) => {
      this.isFetching = isFetching;
    });
    this.messagesService.fetchMessages();
    this.messagesService.start();
  }

  ngOnDestroy(): void {
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
    this.messagesService.stop();
  }
}
