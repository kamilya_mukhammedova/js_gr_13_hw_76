import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../shared/messages.service';
import { MessageData } from '../shared/message.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  messagesUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messagesUploadingSubscription = this.messagesService.messagesUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  postNewMessage() {
     const newMessageData: MessageData = this.form.value;
     this.messagesService.addNewMessage(newMessageData).subscribe(() => {
       this.clearFormValue({
         author: '',
         message: '',
       });
       this.form.controls["author"].markAsUntouched();
       this.form.controls["message"].markAsUntouched();
     });
  }

  clearFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.form.setValue(value);
    });
  }

  ngOnDestroy(): void {
    this.messagesUploadingSubscription.unsubscribe();
  }
}
