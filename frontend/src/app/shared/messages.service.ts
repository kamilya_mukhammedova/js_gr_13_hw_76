import { Injectable } from '@angular/core';
import { map, Subject, tap } from 'rxjs';
import { Message, MessageData } from './message.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messagesUploading = new Subject<boolean>();
  date = '';
  interval = 0;

  private messagesArray: Message[] = [];

  constructor(private http: HttpClient) { }

  fetchMessages() {
    this.messagesFetching.next(true);
    this.http.get<Message[]>( environment.apiUrl + '/messages')
      .pipe(map(result => {
        if (result.length === 0) {
          return [];
        }
        return result.map(message => {
          return new Message(message.id, message.message, message.author, message.datetime);
        });
      }))
      .subscribe(messages => {
        this.messagesArray = messages;
        this.messagesChange.next(this.messagesArray.slice());
        this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
        this.messagesFetching.next(false);
      }, () => {
        this.messagesFetching.next(false);
      });
  }

  addNewMessage(message: MessageData) {
    this.messagesUploading.next(true);
    return this.http.post(environment.apiUrl + '/messages', message)
      .pipe(tap(() => {
        this.messagesUploading.next(false);
      }, () => {
        this.messagesUploading.next(false);
      }));
  }

  start() {
    if(this.interval !== 0) {
      return;
    }
    this.interval = setInterval(() => {
      this.http.get<Message[] | []>(environment.apiUrl + '/messages?datetime=' + this.date)
        .pipe(map(result => {
          if(result.length === 0) {
            return [];
          }
          return result.map(message => {
            console.log(message.datetime)
            return new Message(message.id, message.message, message.author, message.datetime);
          });
        }))
        .subscribe(newMessages => {
          this.messagesArray = this.messagesArray.concat(newMessages);
          this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
          this.messagesChange.next(this.messagesArray.slice());
        }, () => {
          console.log('Something has gone wrong with new messages in chat.');
        });
    },2000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = 0;
  }
}
