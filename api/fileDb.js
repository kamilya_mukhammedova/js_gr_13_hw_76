const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const fileName = './db.json';
let data = [];

module.exports = {
  async init() {
    try {
      const fileContents = await fs.readFile(fileName);
      data = JSON.parse(fileContents.toString());
    } catch (e) {
      data = [];
    }
  },
  getMessages() {
    if (data.length > 30) {
      const indexOfThirtyMessages = data.length - 30;
      return data.slice(indexOfThirtyMessages);
    } else {
      return data;
    }
  },
  getNewMessages(datetime) {
    const lastDateMessage = data.find(p => p.datetime === datetime);
    const indexOfLastMessage = data.indexOf(lastDateMessage);
    if (data.length === (indexOfLastMessage + 1)) {
      return [];
    } else {
      const indexOfFirstNewMessage = indexOfLastMessage + 1;
      return data.slice(indexOfFirstNewMessage);
    }
  },
  addMessage(message) {
    message.id = nanoid();
    message.datetime = new Date().toISOString();
    data.push(message);
    return this.save();
  },
  save() {
    return fs.writeFile(fileName, JSON.stringify(data, null, 2));
  }
};