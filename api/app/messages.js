const express = require('express');
const db = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
  const datetime = req.query.datetime;
  if (datetime) {
    const date = new Date(datetime);
    if (isNaN(date.getDate())) {
      return res.status(400).send({error: 'Invalid datetime'});
    } else {
      return res.send(db.getNewMessages(datetime));
    }
  } else {
    const messages = db.getMessages();
    return res.send(messages);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const newMessage = {
      author: req.body.author,
      message: req.body.message,
    };

    if (req.body.author === '' || req.body.message === '') {
      return res.status(400).send({error: 'Author and message must be present in the request'});
    } else {
      await db.addMessage(newMessage);
      return res.send({message: 'Created new message', id: newMessage.id});
    }
  } catch (e) {
    next(e);
  }
});

module.exports = router;